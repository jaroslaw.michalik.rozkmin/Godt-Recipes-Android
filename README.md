# Godt.no recipies
## Android app

![screenshot](screenshot.png "Screenshot from app")

## App features
* displaying 50 elements from Godt.no API
* storing data offline after sync
* recipe search based on title or ingredients 
(toggle with switch widget next to search field - it need better UX design)
* click to expand item layout - see ingredients

## Code, architecture features

### Kotlin extensions

Apply default schedulers in presenter without breaking rx chain

```kotlin
fun <T> Single<T>.applySchedulers() = 
    subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
```

### Databinding tricks

#### Loading image with Glide from xml
```xml
   app:recipeImageUrl="@{item.image}"
```

```kotlin
    fun loadWithError(view: ImageView, url: String) {
        loadWithPlaceholderAndError(view.context, url, R.drawable.ic_launcher_background, R.drawable.ic_launcher_background, view)
    }
```

#### Changing view state based on ViewModel item state
```xml
android:rotation="@{item.expanded ? 180 : 0}"
android:visibility="@{item.expanded ? View.VISIBLE : View.GONE}"

```

### MVP pattern with default interface
```kotlin
interface BaseView {
    fun showLoading()
    fun hideLoading()
    fun displayError()
}

interface BasePresenter<in BaseView> {
    fun attach(view: BaseView)
    fun detach()
}
```

### Repository pattern and generic Realm implementation
See class BaseDao, RealmDao
```kotlin
override fun add(item: POJO) {
        Realm.getInstance(realmConfig).use {
            it.executeTransaction {
                it.insertOrUpdate(toRealmMapper.map(item))
            }
        }
    }
```

### Gradle variables (mostly dependencies definitions) in separate file
```groovy
imageDependencies = [
            compile: "com.github.bumptech.glide:glide:${GlideVersion}",
            kapt   : "com.github.bumptech.glide:compiler:${GlideVersion}"
    ]
```

```groovy
compile imageDependencies.compile
```

## Summary
There are some unused functionalities and code (e.x providing unused context with Dagger). 
I'm planning to finish this app in the future. 

### Important "to be done" features:
* querying API realtime
* better layouts (another colours and non-default icons)
* UI testing
* detailed view of recipe
* tablet support

### Less important "to be done" features:
* marking recipe as favourite
* navigation inside views, e.x in recipe detail activity there should be link to other recipes with same ingredients

