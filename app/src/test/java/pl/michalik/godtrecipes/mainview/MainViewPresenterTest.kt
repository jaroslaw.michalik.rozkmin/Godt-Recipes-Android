package pl.michalik.godtrecipes.mainview

import com.nhaarman.mockito_kotlin.any
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import pl.michalik.godtrecipes.network.*

/**
 * Created by jaroslawmichalik on 21.12.2017
 */
class MainViewPresenterTest {

    private lateinit var recipesProvider: RecipesProvider
    private lateinit var view: MainViewContract.View
    private lateinit var presenter: MainViewPresenter

    val recipeMockModel = RecipeModel(
            0,
            "title",
            "desc",
            listOf(IngredientModel("a")),
            listOf(Image("a")))

    @Suppress("UNCHECKED_CAST")
    @Before
    fun setup() {

        recipesProvider = Mockito.mock(RecipesProvider::class.java)

        view = Mockito.mock(MainViewContract.View::class.java)
        presenter = MainViewPresenter(recipesProvider, RecipesViewModelMapper())



        Mockito.`when`(recipesProvider.getAll()).thenReturn(Single.just(listOf(recipeMockModel)))
    }

    @Test
    fun testAttachDetach() {

        presenter.attach(view)
        Assert.assertNotNull(presenter.view)

        presenter.detach()
        Assert.assertNull(presenter.view)
    }

    @Test
    fun testSearchAllAndFound() {

        Mockito.`when`(recipesProvider.getAll()).thenReturn(Single.just(listOf(recipeMockModel)))

        presenter.attach(view)

        presenter.searchBy("", MainViewContract.SearchBy.TITLE)

        Mockito.verify(view).addItems(any())
    }


    @Test
    fun testSearchByTitleAndFound() {
        Mockito.`when`(recipesProvider.getBy(RecipeTitleSpecification("asd"))).thenReturn(Single.just(listOf(recipeMockModel)))

        presenter.attach(view)

        presenter.searchBy("asd", MainViewContract.SearchBy.TITLE)

        Mockito.verify(view).addItems(any())
    }

    @Test
    fun testSearchByIngredientsAndFound() {
        Mockito.`when`(recipesProvider.getBy(RecipeIngredientsSpecification("asd"))).thenReturn(Single.just(listOf(recipeMockModel)))

        presenter.attach(view)

        presenter.searchBy("asd", MainViewContract.SearchBy.INGREDIENTS)

        Mockito.verify(view).addItems(any())
    }

    @Test
    fun testSearchByTitleAndNotFound() {
        Mockito.`when`(recipesProvider.getBy(RecipeTitleSpecification("asd"))).thenReturn(Single.error(Throwable("")))

        presenter.attach(view)

        presenter.searchBy("asd", MainViewContract.SearchBy.TITLE)

        Mockito.verify(view).showNotFound()
        Mockito.verify(view).displayError()
    }

    @Test
    fun testSearchByIngredientsAndNotFound() {
        Mockito.`when`(recipesProvider.getBy(RecipeIngredientsSpecification("asd"))).thenReturn(Single.error(Throwable("")))

        presenter.attach(view)

        presenter.searchBy("asd", MainViewContract.SearchBy.INGREDIENTS)

        Mockito.verify(view).showNotFound()
        Mockito.verify(view).displayError()
    }
}