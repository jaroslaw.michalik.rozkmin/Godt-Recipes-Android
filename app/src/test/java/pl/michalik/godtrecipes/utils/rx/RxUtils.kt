package pl.michalik.godtrecipes.utils.rx

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
fun <T> Single<T>.applySchedulers() = doOnSuccess {  }

fun <T> Observable<T>.applySchedulers() = doOnNext {  }

fun <T> Flowable<T>.applySchedulers() = doOnEach {  }
