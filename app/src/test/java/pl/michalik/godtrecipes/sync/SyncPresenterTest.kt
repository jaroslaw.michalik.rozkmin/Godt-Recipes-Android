package pl.michalik.godtrecipes.sync

import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import pl.michalik.godtrecipes.data.BaseDao
import pl.michalik.godtrecipes.network.NetworkService
import pl.michalik.godtrecipes.network.RecipeModel

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class SyncPresenterTest{
    private lateinit var dao: BaseDao<RecipeModel>

    private lateinit var networkService: NetworkService


    private lateinit var view: SyncContract.View

    private lateinit var presenter: SyncPresenter

    @Suppress("UNCHECKED_CAST")
    @Before
    fun setup() {

        networkService = Mockito.mock(NetworkService::class.java)
        dao = Mockito.mock(BaseDao::class.java) as BaseDao<RecipeModel>
        view = Mockito.mock(SyncContract.View::class.java)
        presenter = SyncPresenter(networkService = networkService, recipesDao = dao)
    }

    @Test
    fun testAttachDetach(){

        Mockito.`when`(networkService.getRecipeList()).thenReturn(Single.never())

        presenter.attach(view)
        Assert.assertNotNull(presenter.view)

        presenter.detach()
        Assert.assertNull(presenter.view)
    }

    @Test
    fun testSaveToDatabaseAndShowToViewIfSuccess() {

        val list = listOf(RecipeModel(1))
        Mockito.`when`(networkService.getRecipeList()).thenReturn(Single.just(list))
        presenter.attach(view)


        verify(dao).addAll(list)
    }

    @Test
    fun testShowErrorIfFail(){
        Mockito.`when`(networkService.getRecipeList()).thenReturn(Single.error(Throwable("error")))

        presenter.attach(view)
        verify(view).displayError()
    }

}