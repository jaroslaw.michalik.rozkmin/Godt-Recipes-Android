package pl.michalik.godtrecipes.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import io.realm.RealmConfiguration
import pl.michalik.godtrecipes.data.di.DatabaseModule
import pl.michalik.godtrecipes.detailview.di.DetailViewComponent
import pl.michalik.godtrecipes.detailview.di.DetailViewModule
import pl.michalik.godtrecipes.mainview.di.MainViewComponent
import pl.michalik.godtrecipes.mainview.di.MainViewModule
import pl.michalik.godtrecipes.network.NetworkModule
import pl.michalik.godtrecipes.sync.di.SyncComponent
import pl.michalik.godtrecipes.sync.di.SyncModule
import javax.inject.Singleton

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class, DatabaseModule::class))
interface AppComponent{

    fun plusSyncComponent(syncModule : SyncModule) : SyncComponent
    fun plusMainViewComponent(mainViewModule : MainViewModule) : MainViewComponent
    fun plusDetailViewComponent(detailViewModule : DetailViewModule) : DetailViewComponent

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        @BindsInstance fun realmConfig(realmConfig: RealmConfiguration): Builder
        fun build(): AppComponent
    }

}