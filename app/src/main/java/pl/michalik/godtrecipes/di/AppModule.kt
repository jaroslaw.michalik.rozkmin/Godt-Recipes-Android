package pl.michalik.godtrecipes.di

import android.app.Application
import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
object AppModule {
    lateinit var appComponent: AppComponent

    fun attachApp(application: Application){
        createComponent(application)
    }

    private fun createComponent(app: Application) {
        val realmConfig = getRealmConfig(app)
        appComponent = DaggerAppComponent.builder()
                .realmConfig(realmConfig)
                .application(app)
                .build()
    }

    private fun getRealmConfig(context: Context): RealmConfiguration {
        Realm.init(context)
        return RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
    }

}