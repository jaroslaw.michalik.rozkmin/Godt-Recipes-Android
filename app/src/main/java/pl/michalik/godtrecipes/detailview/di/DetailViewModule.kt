package pl.michalik.godtrecipes.detailview.di

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Module
class DetailViewModule(val activity : Activity){
    @Provides
    fun provideActivity() = activity

    @Provides
    fun provideContext(): Context = activity
}