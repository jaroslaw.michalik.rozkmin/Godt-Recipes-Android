package pl.michalik.godtrecipes.detailview.di

import dagger.Subcomponent

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Subcomponent(modules = arrayOf(DetailViewModule::class))
interface DetailViewComponent