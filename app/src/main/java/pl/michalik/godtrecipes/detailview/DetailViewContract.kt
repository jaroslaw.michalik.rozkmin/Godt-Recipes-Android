package pl.michalik.godtrecipes.detailview

import pl.michalik.godtrecipes.utils.view.BasePresenter
import pl.michalik.godtrecipes.utils.view.BaseView

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface DetailViewContract{
    interface View : BaseView{
        fun addElement(elementViewModel : ElementViewModel)
    }
    interface Presenter : BasePresenter<View>
}