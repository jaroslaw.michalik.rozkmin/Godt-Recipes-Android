package pl.michalik.godtrecipes.detailview

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
abstract class ElementViewModel{

}

data class HeaderViewModel(val title : String, val image : String)

data class IngredientsViewModel(val ingredientList : List<String>)

data class StepsViewModel(val steps : List<String>)