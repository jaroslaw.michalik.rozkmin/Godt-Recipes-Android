package pl.michalik.godtrecipes.mainview

import pl.michalik.godtrecipes.data.Mapper
import pl.michalik.godtrecipes.network.IngredientModel
import pl.michalik.godtrecipes.network.RecipeModel
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class RecipesViewModelMapper @Inject constructor() : Mapper<RecipeModel, RecipeListItemViewModel>{
    override fun map(from: RecipeModel): RecipeListItemViewModel {
        return RecipeListItemViewModel(from.id, from.title, from.images.first().url, createSimpleIngredients(from.ingredients), false)
    }

    private fun createSimpleIngredients(ingredients: List<IngredientModel>): List<IngredientViewModel> {
        return ingredients.map {
            val content = it.elements.map { "${it.name} : ${it.amount} [${it.symbol}]" }
            val sb : StringBuilder = StringBuilder()

            content.forEach{
                sb.append(it)
                sb.append("\n")
            }

            IngredientViewModel(it.name, sb.toString())
        }
    }

}