package pl.michalik.godtrecipes.mainview

import io.reactivex.Single
import io.reactivex.disposables.Disposable
import pl.michalik.godtrecipes.data.Mapper
import pl.michalik.godtrecipes.network.RecipeIngredientsSpecification
import pl.michalik.godtrecipes.network.RecipeModel
import pl.michalik.godtrecipes.network.RecipeTitleSpecification
import pl.michalik.godtrecipes.utils.rx.applySchedulers
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class MainViewPresenter @Inject constructor(val provider: RecipesProvider, val mapper: Mapper<RecipeModel, RecipeListItemViewModel>) : MainViewContract.Presenter {

    var view: MainViewContract.View? = null
    private var disposable: Disposable? = null

    val toViewModel: (List<RecipeModel>) -> List<RecipeListItemViewModel> = { it.map { mapper.map(it) } }

    override fun attach(view: MainViewContract.View) {
        this.view = view
    }

    override fun detach() {
        if(disposable!=null){
            disposable?.dispose()
        }
        this.view = null
    }

    override fun searchBy(query: String, searchBy: MainViewContract.SearchBy) {
        if (query.isNullOrEmpty()) {
            disposable = searchAll()
        } else {
            disposable = when (searchBy) {
                MainViewContract.SearchBy.TITLE -> searchByTitleQuery(query)
                MainViewContract.SearchBy.INGREDIENTS -> searchByIngredients(query)
            }
        }
    }

    private fun searchByIngredients(query: String): Disposable? =
            provider.getBy(RecipeIngredientsSpecification(query = query)).load()


    private fun searchByTitleQuery(query: String): Disposable? =
            provider.getBy(RecipeTitleSpecification(query = query)).load()


    fun searchAll(): Disposable? = provider.getAll().load()

    private fun Single<List<RecipeModel>>.load(): Disposable? =
            applySchedulers()
                    .map(toViewModel)
                    .doOnSubscribe { view?.showLoading() }
                    .subscribe(
                            {
                                if (it.isNotEmpty()) view?.addItems(it)
                                else view?.showNotFound()
                                view?.hideLoading()
                            },
                            {
                                view?.showNotFound()
                                view?.displayError()
                                view?.hideLoading()
                            })


}

