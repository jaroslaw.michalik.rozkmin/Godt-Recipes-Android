package pl.michalik.godtrecipes.mainview

import io.reactivex.Observable
import io.reactivex.Single
import pl.michalik.godtrecipes.data.BaseDao
import pl.michalik.godtrecipes.data.Specification
import pl.michalik.godtrecipes.network.RecipeModel
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class RecipesProviderImpl @Inject constructor(val dao: BaseDao<RecipeModel>) : RecipesProvider{
    override fun getAll(): Single<List<RecipeModel>> = Observable.fromIterable(dao.findAll()).toList()

    override fun getBy(specification: Specification): Single<List<RecipeModel>> = Observable.fromIterable(dao.query(specification)).toList()

    override fun getOne(id: Int): Single<RecipeModel> = Observable.just(dao.findById(id)).toList().map { it[0] }
}