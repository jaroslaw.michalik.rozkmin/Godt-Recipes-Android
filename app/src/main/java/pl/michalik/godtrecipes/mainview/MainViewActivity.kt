package pl.michalik.godtrecipes.mainview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import kotlinx.android.synthetic.main.search_view.*
import pl.michalik.godtrecipes.R
import pl.michalik.godtrecipes.di.AppModule
import pl.michalik.godtrecipes.mainview.di.MainViewModule
import javax.inject.Inject
import android.app.SearchManager
import android.view.View
import android.widget.SearchView
import android.widget.Switch
import android.widget.Toast


/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class MainViewActivity : AppCompatActivity(), MainViewContract.View {

    @Inject
    lateinit var presenter: MainViewContract.Presenter

    @Inject
    lateinit var adapter: RecipeListAdapter

    companion object {
        val TAG = MainViewActivity::class.java.simpleName
        const val QUERY_KEY = "QUERY"
        //start activity with predefined search query
        fun newInstance(query: String, context: Context): Intent {
            val intent = Intent(context, MainViewActivity::class.java)
            intent.putExtra(QUERY_KEY, query)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppModule.appComponent
                .plusMainViewComponent(MainViewModule(this))
                .inject(this)

        setContentView(R.layout.search_view)

        search_view_recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        search_view_recycler.adapter = adapter

        presenter.attach(this)
        presenter.searchBy(intent.extras.getString(QUERY_KEY), MainViewContract.SearchBy.TITLE)
    }

    private var searchBy: MainViewContract.SearchBy = MainViewContract.SearchBy.TITLE

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.options_menu, menu)

        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val search = menu?.findItem(R.id.search)?.actionView as SearchView

        search.setSearchableInfo(manager.getSearchableInfo(componentName))

        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true
            override fun onQueryTextChange(query: String): Boolean {
                presenter.searchBy(query, searchBy)
                return true
            }
        })

        val aSwitch = menu.findItem(R.id.search_toggle)?.actionView as Switch
        aSwitch.setOnCheckedChangeListener {
            _, isChecked ->
            searchBy = if (isChecked) MainViewContract.SearchBy.INGREDIENTS else MainViewContract.SearchBy.TITLE
        }

        return true

    }

    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }


    override fun showLoading() {
        search_progress_loader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        search_progress_loader.visibility = View.GONE
    }

    override fun displayError() {
        Toast.makeText(this, R.string.search_no_results_error, Toast.LENGTH_SHORT).show()
    }

    override fun showNotFound() {
        search_view_recycler.visibility = View.GONE
        search_view_not_found.visibility = View.VISIBLE
    }

    override fun addItems(list: List<RecipeListItemViewModel>) {

        search_view_recycler.visibility = View.VISIBLE
        search_view_not_found.visibility = View.GONE

        adapter.items = list
    }

}