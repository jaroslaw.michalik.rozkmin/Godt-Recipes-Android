package pl.michalik.godtrecipes.mainview

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.michalik.godtrecipes.databinding.RecipeSearchItemBinding
import pl.michalik.godtrecipes.network.IngredientModel
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class RecipeListAdapter @Inject constructor() : RecyclerView.Adapter<RecipeListAdapter.RecipeListViewHolder>() {

    var items = listOf<RecipeListItemViewModel>()
    set(value) {
        field=value
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecipeListViewHolder?, position: Int) {
        holder?.apply {
            bind(items[position])
        }

        holder?.binding?.recipeSearchItemToggle?.setOnClickListener{
            items[position].expanded=items[position].expanded.not()
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecipeListViewHolder =
        RecipeListViewHolder(RecipeSearchItemBinding.inflate(LayoutInflater.from(parent?.context), parent, false))


    inner class RecipeListViewHolder(val binding : RecipeSearchItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(itemViewModel: RecipeListItemViewModel){
            binding.item=itemViewModel
            binding.recipeSearchItemIngredients.layoutManager=LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            binding.recipeSearchItemIngredients.adapter = IngredientsAdapter(itemViewModel.ingredients)
            binding.executePendingBindings()
        }
    }
}