package pl.michalik.godtrecipes.mainview

import pl.michalik.godtrecipes.utils.view.BasePresenter
import pl.michalik.godtrecipes.utils.view.BaseView

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface MainViewContract{

    enum class SearchBy{
        TITLE, INGREDIENTS
    }

    interface View : BaseView{
        fun addItems(list : List<RecipeListItemViewModel>)
        fun showNotFound()
    }
    interface Presenter : BasePresenter<View>{
        fun searchBy(query : String, searchBy: SearchBy)
    }
}