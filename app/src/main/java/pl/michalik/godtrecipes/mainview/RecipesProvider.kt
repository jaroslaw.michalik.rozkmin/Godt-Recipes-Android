package pl.michalik.godtrecipes.mainview

import io.reactivex.Single
import pl.michalik.godtrecipes.data.Specification
import pl.michalik.godtrecipes.network.RecipeModel

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface RecipesProvider{
    fun getAll() : Single<List<RecipeModel>>
    fun getBy(specification: Specification) : Single<List<RecipeModel>>
    fun getOne(id : Int) : Single<RecipeModel>
}