package pl.michalik.godtrecipes.mainview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.michalik.godtrecipes.databinding.IngredientItemBinding

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class IngredientsAdapter(val ingredients : List<IngredientViewModel>) : RecyclerView.Adapter<IngredientsAdapter.IngredientViewHolder>() {
    override fun getItemCount(): Int = ingredients.size

    override fun onBindViewHolder(holder: IngredientViewHolder?, position: Int) {
        holder?.apply{
            bind(ingredients[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): IngredientViewHolder =
        IngredientViewHolder(IngredientItemBinding.inflate(LayoutInflater.from(parent?.context), parent, false))


    inner class IngredientViewHolder(val binding: IngredientItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item : IngredientViewModel){
            binding.item=item
            binding.executePendingBindings()
        }
    }
}


