package pl.michalik.godtrecipes.mainview

import android.databinding.BindingAdapter
import android.widget.ImageView
import pl.michalik.godtrecipes.R
import pl.michalik.godtrecipes.utils.view.loadWithPlaceholderAndError

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
data class RecipeListItemViewModel(
        val id : Int,
        val title : String,
        val image : String,
        val ingredients : List<IngredientViewModel>,
        var expanded : Boolean)

@BindingAdapter("bind:recipeImageUrl")
fun loadWithError(view: ImageView, url: String) {
    loadWithPlaceholderAndError(view.context, url, R.drawable.ic_launcher_background, R.drawable.ic_launcher_background, view)
}


data class IngredientViewModel(val title : String, val content : String)




