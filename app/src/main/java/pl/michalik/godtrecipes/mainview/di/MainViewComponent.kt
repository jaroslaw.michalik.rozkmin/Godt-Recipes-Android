package pl.michalik.godtrecipes.mainview.di

import dagger.Subcomponent
import pl.michalik.godtrecipes.mainview.MainViewActivity

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Subcomponent(modules = arrayOf(MainViewModule::class))
interface MainViewComponent{
    fun inject(mainViewActivity: MainViewActivity)
}