package pl.michalik.godtrecipes.mainview.di

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.michalik.godtrecipes.data.Mapper
import pl.michalik.godtrecipes.mainview.*
import pl.michalik.godtrecipes.network.RecipeModel

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Module
class MainViewModule(val activity : Activity){
    @Provides
    fun provideActivity() = activity

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun providePresenter(presenter : MainViewPresenter) : MainViewContract.Presenter = presenter

    @Provides
    fun provideRecipesProvider(impl : RecipesProviderImpl) : RecipesProvider = impl

    @Provides
    fun provideMapper(viewModelMapper : RecipesViewModelMapper) : Mapper<RecipeModel, RecipeListItemViewModel> = viewModelMapper
}