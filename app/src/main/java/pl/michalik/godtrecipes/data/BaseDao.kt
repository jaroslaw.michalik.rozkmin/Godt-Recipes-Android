package pl.michalik.godtrecipes.data

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface BaseDao<T> {
    fun add(item: T)
    fun addAll(items: List<T>)
    fun findAll(): List<T>
    fun findById(id: Int) : T
    fun remove(specification: Specification)
    fun removeAll()
    fun query(specification: Specification): List<T>
}
/**
 * Specification - use it to query repository results
 */
interface Specification //only marker interface

/**
 * Mapper
 * @param From - object to be mapped
 * @param To - target object
 */
interface Mapper<From, To> {
    fun map(from: From): To
}
