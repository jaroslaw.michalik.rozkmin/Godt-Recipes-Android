package pl.michalik.godtrecipes.data.di

import dagger.Module
import dagger.Provides
import pl.michalik.godtrecipes.data.BaseDao
import pl.michalik.godtrecipes.data.RecipeDao
import pl.michalik.godtrecipes.network.RecipeModel

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Module
class DatabaseModule {
    @Provides
    fun provideRecipeDao(recipeDao: RecipeDao) : BaseDao<RecipeModel> = recipeDao
}