package pl.michalik.godtrecipes.data

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@RealmClass
open class RealmRecipe(
        @PrimaryKey
        var id: Int = 0,
        var title: String = "",
        var description: String = "",
        var ingredients: RealmList<RealmIngredient> = RealmList(),
        var images: RealmList<RealmImage> = RealmList()) : RealmObject()

@RealmClass
open class RealmIngredient(
        var name: String = "",
        var elements: RealmList<RealmElement> = RealmList()) : RealmObject()

@RealmClass
open class RealmElement(
        var amount: Double = 0.0,
        var name: String = "",
        var unitName: String = "",
        var symbol: String = "") : RealmObject()

@RealmClass
open class RealmImage(var url: String = "") : RealmObject()