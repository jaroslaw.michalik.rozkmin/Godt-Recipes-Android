package pl.michalik.godtrecipes.data

import io.realm.Case
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmList
import pl.michalik.godtrecipes.network.*
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */

class RecipeDao @Inject constructor(
        realmConfiguration: RealmConfiguration,
        recipeToRealmMapper: RecipeToRealmMapper,
        realmToRecipeMapper: RealmToRecipeMapper) : RealmDao<RecipeModel, RealmRecipe>(
        realmClass = RealmRecipe::class.java,
        realmConfig = realmConfiguration,
        toRealmMapper = recipeToRealmMapper,
        toPojoMapper = realmToRecipeMapper) {
    override fun query(specification: Specification): List<RecipeModel> {
        if (specification is RecipeTitleSpecification) {
            Realm.getInstance(realmConfig).use {
                return it.where(realmClass)
                        .contains("title", specification.query, Case.INSENSITIVE)
                        .findAll()
                        .map {
                            toPojoMapper.map(it)
                        }
            }
        } else if (specification is RecipeIngredientsSpecification) {
            Realm.getInstance(realmConfig).use {
                return it.where(realmClass)
                        .contains("ingredients.elements.name", specification.query)
                        .findAll()
                        .map { toPojoMapper.map(it) }
            }
        }

        return listOf()
    }
}

class RecipeToRealmMapper @Inject constructor() : Mapper<RecipeModel, RealmRecipe> {
    override fun map(from: RecipeModel): RealmRecipe {
        val ingredients = from.ingredients
                .map {
                    val elements = it.elements.map {
                        RealmElement(
                                it.amount,
                                it.name,
                                it.unitName,
                                it.symbol)
                    }
                    val realmElements = RealmList<RealmElement>()
                    realmElements.addAll(elements)
                    RealmIngredient(
                            it.name,
                            realmElements)
                }
        val realmIngredients = RealmList<RealmIngredient>()
        realmIngredients.addAll(ingredients)

        val images = from.images.map { RealmImage(it.url) }
        val realmImages = RealmList<RealmImage>()
        realmImages.addAll(images)

        return RealmRecipe(from.id, from.title, from.description, realmIngredients, realmImages)
    }
}

class RealmToRecipeMapper @Inject constructor() : Mapper<RealmRecipe, RecipeModel> {
    override fun map(from: RealmRecipe): RecipeModel = RecipeModel(
            from.id,
            from.title,
            from.description,
            from.ingredients.map {
                IngredientModel(
                        it.name,
                        it.elements.map { Element(it.amount, "", it.name, it.unitName, it.symbol) })
            },
            from.images.map { Image(it.url) })

}