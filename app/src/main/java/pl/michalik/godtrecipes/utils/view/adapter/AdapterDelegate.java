package pl.michalik.godtrecipes.utils.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by michalik on 20.05.17
 */

public interface AdapterDelegate<T> {

    boolean isForViewType(@NonNull T items, int position);

    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(int position, @NonNull RecyclerView.ViewHolder holder);

}

