package pl.michalik.godtrecipes.utils.view

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface BaseView {
    fun showLoading()
    fun hideLoading()
    fun displayError()
}

interface BasePresenter<in BaseView> {
    fun attach(view: BaseView)
    fun detach()
}