package pl.michalik.godtrecipes.utils.view.adapter;

import android.app.Activity;

import java.util.Collection;

/**
 * Created by michalik on 06.08.17
 */

@SuppressWarnings("WeakerAccess")
public abstract class AbstractDelegate<T extends Collection> implements  AdapterDelegate<T>{
    public Activity activity;
    public T items;

    public AbstractDelegate(Activity activity, T items){
        this.activity = activity;
        this.items = items;
    }
}
