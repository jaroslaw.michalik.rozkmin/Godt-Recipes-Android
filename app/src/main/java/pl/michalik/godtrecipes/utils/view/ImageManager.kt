package pl.michalik.godtrecipes.utils.view

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
fun loadWithPlaceholderAndError(context: Context, url: String, placeholderRes: Int, errorRes: Int, imageView: ImageView) {
    val options = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
    if (placeholderRes > 0) {
        options.placeholder(placeholderRes)
    }
    if (errorRes > 0) {
        options.error(errorRes)
    }
    loadImage(context, url, options, imageView)
}

private fun loadImage(context: Context, url: String, options: RequestOptions, imageView: ImageView) {
    Glide.with(context)
            .load(url)
            .apply(options)
            .into(imageView)
}