package pl.michalik.godtrecipes

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import pl.michalik.godtrecipes.mainview.MainViewActivity
import pl.michalik.godtrecipes.sync.SyncActivity



class MainActivity : AppCompatActivity() {

    companion object {
        val TAG = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        if(shouldSync()){
            startActivityForResult(Intent(this, SyncActivity::class.java), 0)
        }
        else{
            startMainView()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(resultCode){
            SyncActivity.SUCCESS -> {
                saveSyncDone()
                startMainView()
            }
            SyncActivity.FAILURE -> displayError()
        }
    }

    private fun displayError() {
        Toast.makeText(this, R.string.sync_failure, Toast.LENGTH_SHORT).show()
    }

    private fun startMainView() {
        Log.d(TAG, "staring main application...")
        startActivity(MainViewActivity.newInstance("", this))
        finish()
    }

    private fun shouldSync() : Boolean{
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        return sharedPref.getBoolean("should_sync", true)
    }

    private fun saveSyncDone(){
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        sharedPref.edit()
                .putBoolean("should_sync", false)
                .apply()
    }


}
