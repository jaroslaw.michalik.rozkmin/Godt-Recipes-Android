package pl.michalik.godtrecipes.sync

import pl.michalik.godtrecipes.utils.view.BasePresenter
import pl.michalik.godtrecipes.utils.view.BaseView

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface SyncContract{
    interface View : BaseView{
        fun finishSync(result : Int)
    }
    interface Presenter : BasePresenter<View>{
        fun retry()
    }
}