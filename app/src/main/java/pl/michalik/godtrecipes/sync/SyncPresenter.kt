package pl.michalik.godtrecipes.sync

import io.reactivex.disposables.Disposable
import pl.michalik.godtrecipes.data.BaseDao
import pl.michalik.godtrecipes.network.NetworkService
import pl.michalik.godtrecipes.network.RecipeModel
import pl.michalik.godtrecipes.utils.rx.applySchedulers
import javax.inject.Inject

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class SyncPresenter @Inject constructor(val networkService: NetworkService, val recipesDao: BaseDao<RecipeModel>) : SyncContract.Presenter {

    companion object {
        val TAG = SyncPresenter::class.java.simpleName
    }

    var disposable: Disposable? = null
    var view: SyncContract.View? = null

    override fun attach(view: SyncContract.View) {
        this.view = view
        loadData()
    }

    private fun loadData() {
        disposable = networkService.getRecipeList()
                .map{ recipesDao.addAll(it) }
                .applySchedulers()
                .doOnSubscribe { view?.showLoading() }
                .doOnDispose { view?.hideLoading() }
                .subscribe({
                    view?.hideLoading()
                    view?.finishSync(SyncActivity.SUCCESS)
                }, {
                    view?.hideLoading()
                    view?.displayError()
                    view?.finishSync(SyncActivity.FAILURE)
                })
    }

    override fun detach() {
        this.view = null
        if(disposable!=null){
            disposable?.dispose()
        }
    }

    override fun retry() {
        loadData()
    }

}