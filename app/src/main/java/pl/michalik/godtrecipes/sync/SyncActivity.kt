package pl.michalik.godtrecipes.sync

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.sync_activity.*
import pl.michalik.godtrecipes.R
import pl.michalik.godtrecipes.di.AppModule
import pl.michalik.godtrecipes.sync.di.SyncModule
import javax.inject.Inject
import android.content.Intent



/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class SyncActivity : AppCompatActivity(), SyncContract.View {

    companion object {
        const val SUCCESS = 1
        const val FAILURE = 0
        val TAG = SyncActivity::class.java.simpleName
    }

    @Inject
    lateinit var presenter: SyncContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppModule.appComponent
                .plusSyncComponent(SyncModule(this))
                .inject(this)

        setContentView(R.layout.sync_activity)

        presenter.attach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun showLoading() {
        sync_activity_progress_loader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        sync_activity_progress_loader.visibility = View.GONE
    }

    override fun displayError() {
        Toast.makeText(this, R.string.sync_failure, Toast.LENGTH_SHORT).show()
    }

    override fun finishSync(result: Int)  {
        Log.d(TAG, "finishing...")
        val intent = Intent()
        setResult(result, intent)
        finish()
    }

}