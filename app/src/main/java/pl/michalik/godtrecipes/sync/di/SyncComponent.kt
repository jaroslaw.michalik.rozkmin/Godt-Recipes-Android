package pl.michalik.godtrecipes.sync.di

import dagger.Subcomponent
import pl.michalik.godtrecipes.sync.SyncActivity

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Subcomponent(modules = arrayOf(SyncModule::class))
interface SyncComponent {
    fun inject(activity: SyncActivity)
}
