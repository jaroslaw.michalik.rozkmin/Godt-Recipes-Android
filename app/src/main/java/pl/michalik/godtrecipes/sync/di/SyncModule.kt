package pl.michalik.godtrecipes.sync.di

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.michalik.godtrecipes.sync.SyncContract
import pl.michalik.godtrecipes.sync.SyncPresenter

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
@Module
class SyncModule(val activity: Activity) {
    @Provides
    fun provideActivity() = activity

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun providePresenter(syncPresenter: SyncPresenter) : SyncContract.Presenter = syncPresenter

}