package pl.michalik.godtrecipes

import android.app.Application
import pl.michalik.godtrecipes.di.AppModule

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class App : Application(){
    override fun onCreate() {
        super.onCreate()
        AppModule.attachApp(this)
    }
}