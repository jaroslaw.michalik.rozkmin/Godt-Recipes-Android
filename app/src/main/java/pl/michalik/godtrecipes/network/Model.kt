package pl.michalik.godtrecipes.network

import pl.michalik.godtrecipes.data.Specification

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
class RecipeModel(
        var id: Int = 0,
        var title: String = "",
        var description: String = "",
        var ingredients: List<IngredientModel> = listOf(),
        var images: List<Image> = listOf())

class IngredientModel(
        var name: String = "",
        var elements: List<Element> = listOf())

class Element(
        var amount: Double = 0.0,
        var hint: String = "",
        var name: String = "",
        var unitName: String = "",
        var symbol: String = "")

class Image(var url: String = "")

data class RecipeTitleSpecification(val query: String) : Specification
data class RecipeIngredientsSpecification(val query: String) : Specification