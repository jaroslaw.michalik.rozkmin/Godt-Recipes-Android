package pl.michalik.godtrecipes.network

import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by jaroslawmichalik on 20.12.2017
 */
interface NetworkService{
    @GET("api/getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1&limit=50&from=0")
    fun getRecipeList() : Single<List<RecipeModel>>
}